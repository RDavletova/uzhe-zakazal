module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',  [
      'jest-html-reporters',
      {
        filename: 'report.html',
        expand: true,
        pageTitle: 'Matreshka_Report'
      }
    ]
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/ma*.spec.*'] , // ['**/specs/*.spec.*']
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
  setupFilesAfterEnv: ["jest-allure/dist/setup"]
  
};
